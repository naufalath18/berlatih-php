<!DOCTYPE html>
<html>
<head>
    <title>Ubah Huruf</title>
</head>
<body>
    <h1>Ubah Huruf</h1>
    <?php
    function ubah_huruf($string){
        $arr = str_split($string);
        
        $kata = '';
        foreach($arr as $word)
        {
            $kata = $kata . ++$word;
        }

        echo ($kata).'<br>';
    }

    // TEST CASES
    echo ubah_huruf('wow'); // xpx
    echo ubah_huruf('developer'); // efwfmpqfs
    echo ubah_huruf('laravel'); // mbsbwfm
    echo ubah_huruf('keren'); // lfsfo
    echo ubah_huruf('semangat'); // tfnbohbu

    ?>
</body>
</html>